extends Node


var contador = 0


func _carrega_cena(caminho):
	var carregada = ResourceLoader.load(caminho)
	if carregada:
		var nova_cena = carregada.instantiate()
		get_tree().root.add_child(nova_cena)
		get_tree().current_scene.queue_free()
		get_tree().current_scene = nova_cena
