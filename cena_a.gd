extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$MudaB.connect("pressed", _on_MudaB_click)
	$Valor.text = str(Controlador.contador)

func _on_MudaB_click():
	Controlador._carrega_cena("res://cena_b.tscn")
